# frozen_string_literal: true

# Gem Version
module OptParseValidator
  VERSION = '1.8.1'
end
